# -*- coding: utf-8 -*-

__author__ = 'Jure Hostnik'


import requests
import re
import html
import math
import datetime


bb = set() # names of bands that are not in 'bands'
cc = set() # countries that are not in 'countries'

def create_date(*args):
    '''
    input: ('yyyy-mm-dd') or ('dd', 'month', 'yyyy')
    output: datetime.date object or None
    '''
    months = {'January' : 1,
          'February' : 2,
          'March' : 3,
          'April' : 4,
          'May' : 5,
          'June' :6,
          'July' : 7,
          'August' : 8,
          'September' : 9,
          'October' : 10,
          'November' : 11,
          'December' : 12}
    if len(args) == 1:
        return datetime.date(*[int(i) for i in args[0].split('-')])
    else:
        try:
            return datetime.date(int(args[-1]), months[args[1]], int(args[0]))
        except ValueError:
            return None

class Festivals:
    def __init__(self):
        # http://en.concerts-metal.com/
        fest_links = [
            'http://en.concerts-metal.com/festivals_metal_2004.html',
            'http://en.concerts-metal.com/festivals_metal_2005.html',
            'http://en.concerts-metal.com/festivals_metal_2006.html',
            'http://en.concerts-metal.com/festivals_metal_2007.html',
            'http://en.concerts-metal.com/festivals_metal_2008.html',
            'http://en.concerts-metal.com/festivals_metal_2009.html',
            'http://en.concerts-metal.com/festivals_metal_2010.html',
            'http://en.concerts-metal.com/festivals_metal_2011.html',
            'http://en.concerts-metal.com/festivals.php',
            'http://en.concerts-metal.com/festivals-2013.php',
            'http://en.concerts-metal.com/festivals-2014.php',
            'http://en.concerts-metal.com/festivals-2015.php',
            'http://en.concerts-metal.com/festivals-2016.php',
            'http://en.concerts-metal.com/festivals-2017.php']

        self.fest_list = list() # (_name, _location, _country, link->info)
        self.fest_info = list() # [name, (bands), (begin_date, end_date), price, web]
        self.loc_list = list()  # (location, address, postal_code, country)

        # self.fest_list
        for link in fest_links:
            p = html.unescape(requests.get(link).text)
            self.fest_list.extend(re.findall('''<a title="(.*) - (.*) - (.*) - \d{2}/\d{2}/\d{4}" \n\t\t\thref="(.*)">.*</a>''', p))
            
        # self.fest_info, self.loc_list
        for fest in self.fest_list:
            q = html.unescape(requests.get('http://en.concerts-metal.com/' + fest[-1]).text)
            self.fest_info.append([fest[0].strip(),
                              set(b.strip() for b in re.findall('''<font itemprop="name" size=4><a title="(.*?) en concert" href="search.php\?g=\d+">''', q)),
                              re.findall('''<meta itemprop="startDate" content="(\d{4}-\d{2}-\d{2})">\w{6,9} <?b?>?\d{1,2}<?/?b?>? \w{3,9} [\w|\s|\d]*>? ? ?\w{,9} ?<?b?>?(\d{,2})<?/?b?>? ?(\w{,9}) ?(\d{,4})<br/>''', q),
                              re.findall('''Presale : (\d*). (\w*)\xa0\xa0A?t? ?t?h?e? ?s?h?o?w? ?:? ?(\d*).? ?(\w*)<br/>''', q),
                              re.findall('''Event's website :  : <a target="_blank" title="'''+fest[0]+'''" href="(.*?)"><img''', q)])
            self.loc_list.append([fest[1].strip(),
                             re.findall('''<span itemprop="streetAddress">(.*?)</span>''', q),
                             re.findall('''<span itemprop="postalCode">(\d*)</span>''', q),
                             fest[2].strip()])
            cc.add(fest[2])

        # bb, rearranging data
        for i, fest in enumerate(self.fest_info):
            # bb
            bb.update(fest[1])
            # dates
            try:
                fest[2] = (create_date(*fest[2][0][:1]), create_date(*fest[2][0][1:]))
            except IndexError as napaka:
                print(fest)
                raise napaka
            # tickets
            if len(fest[3]) > 0:
                presale = ' '.join(fest[3][0][:2]).strip()
                at_show = ' '.join(fest[3][0][2:]).strip()
                if len(presale) > 0:
                    presale = 'presale: ' + presale
                if len(at_show) > 0:
                    at_show = ', at the show: ' + at_show
                fest[3] = presale + at_show
            else:
                fest[3] = None
            # web
            if len(fest[-1]) > 0:
                fest[-1] = fest[-1][0].strip()
            else:
                fest[-1] = None
            self.fest_info[i] = fest

        # rearranging data
        for i, loc in enumerate(self.loc_list):
            if len(loc[1]) > 0:
                loc[1] = loc[1][0].strip()
            else:
                loc[1] = None
            if len(loc[2]) > 0:
                loc[2] = int(loc[2][0].strip())
            else:
                loc[2] = None
            self.loc_list[i] = tuple(loc)


class Bands():
    def __init__(self):
        # http://www.metal-archives.com/
        self.bands = list() # [name, country, [genres], status, info]
        self.countries = set()
        self.genres = set()

        a = ['NBR', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
             'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

        # bands, countries, genres
        for i in a:
            pars = {'sEcho' : 2,
                    'iColumns' : 4,
                    'sColumns' : '',
                    'iDisplayStart' : 0,
                    'iDisplayLength' : 500,
                    'mDataProp_0' : 0,
                    'mDataProp_1' : 1,
                    'mDataProp_2' : 2,
                    'mDataProp_3' : 3,
                    'iSortCol_0' : 0,
                    'sSortDir_0' : 'asc',
                    'iSortingCols' : 1,
                    'bSortable_0' : 'true',
                    'bSortable_1' : 'true',
                    'bSortable_2' : 'true',
                    'bSortable_3' : 'false',
                    '_' : 1461236930839}
            
            r = requests.get('http://www.metal-archives.com/browse/ajax-letter/l/' + i + '/json/1', pars)
            s = r.json(encoding='utf-8')
            n = math.ceil(s.get('iTotalRecords') / 500)

            for j in range(n + 1):
                if j > 0:
                    pars['iDisplayStart'] = j * 500
                    r = requests.get('http://www.metal-archives.com/browse/ajax-letter/l/' + i + '/json/1', pars)
                    s = r.json(encoding='utf-8')
            
                for b in s.get('aaData'):
                    self.bands.append([re.findall('''<a href='http://www.metal-archives.com/bands/.*'>(.*)</a>''', b[0])[0],
                                  b[1],
                                  b[2].split(', '),
                                  re.findall('''<span class=".*">(.*)</span>''', b[3])[0],
                                  re.findall('''<a href='(.*)'>.*</a>''', b[0])[0]])
                    self.countries.add(b[1])
                    self.genres.update(b[2].split(', '))

        for c in cc:
            if c not in self.countries:
                self.countries.add(c)

        band_names = [band[0] for band in self.bands] 
        for b in bb:
            if b not in band_names:
                self.bands.append([b, None, [], None, None])
