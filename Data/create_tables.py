#!/usr/bin/env python
#-*- coding: utf-8 -*-

__author__ = 'Jure Hostnik'


from auth import *
import psycopg2, psycopg2.extensions, psycopg2.extras


psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s šumniki
conn = psycopg2.connect(database = db, host = host, user = user, password = password)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # onemogočimo transakcije
cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

def create_tables():
    cur.execute('''
        CREATE TABLE country (
                name TEXT PRIMARY KEY);
                
        CREATE TABLE genre (
                name TEXT PRIMARY KEY);
                
        CREATE TABLE location (
                id SERIAL PRIMARY KEY,
                name TEXT NOT NULL,
                address TEXT,
                postcode INTEGER,
                country TEXT
                        REFERENCES country(name));
                        
        CREATE TABLE band (
                id SERIAL PRIMARY KEY,
                name TEXT NOT NULL,
                status TEXT,
                info TEXT,
                country TEXT
                        REFERENCES country(name));
                        
        CREATE TABLE festival (
                id SERIAL PRIMARY KEY,
                name TEXT NOT NULL,
                begin_date DATE NOT NULL,
                end_date DATE,
                price TEXT,
                web TEXT,
                location INTEGER NOT NULL
                        REFERENCES location(id));
                        
        CREATE TABLE band_genre (
                band INTEGER NOT NULL
                        REFERENCES band(id),
                genre TEXT
                        REFERENCES genre(name),
                PRIMARY KEY(band, genre));
                
        CREATE TABLE band_festival (
                band INTEGER NOT NULL
                        REFERENCES band(id),
                festival INTEGER NOT NULL
                        REFERENCES festival(id),
                PRIMARY KEY(band, festival));''')

##create_tables()
