#!/usr/bin/env python
#-*- coding: utf-8 -*-

__author__ = 'Jure Hostnik'


from auth import *
import psycopg2, psycopg2.extensions, psycopg2.extras
from get_data import Festivals, Bands
import time


t0 = time.clock()

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
conn = psycopg2.connect(database = db, host = host, user = user, password = password)
cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

fests = Festivals()
bands = Bands()

tables = ['band',
          'band_festival',
          'band_genre',
          'country',
          'festival',
          'genre',
          'location']

def empty(table):
    cur.execute('TRUNCATE ' + table + ' RESTART IDENTITY CASCADE;')
    conn.commit()

def empty_all():
    for t in tables:
        empty(t)
    conn.commit()

##empty_all()

def fill():
    for c in bands.countries:
        cur.execute('''INSERT INTO country VALUES (%s);''', [c])

    for g in bands.genres:
        cur.execute('''INSERT INTO genre VALUES (%s);''', [g])

    for l in set(fests.loc_list):
        cur.execute('''INSERT INTO location (name, address, postcode, country)
                       VALUES (%s, %s, %s, %s);''', l)

    for b in bands.bands:
        #!!!
        cur.execute('''INSERT INTO band (name, country, status, info)
                       VALUES (%s, %s, %s, %s)
                       RETURNING id;''', b[:2] + b[3:])
        b_id = cur.fetchone()[0]
        for g in b[2]:
            cur.execute('''INSERT INTO band_genre VALUES (%s, %s);''',
                        (b_id, g))
                
    for i, f in enumerate(fests.fest_info):
        cur.execute('''WITH loc AS (
                           SELECT id FROM location
                           WHERE (name, address, postcode, country) IS NOT DISTINCT FROM %s)
                       INSERT INTO festival (name, begin_date, end_date, price, web, location)
                       VALUES (%s, %s, %s, %s, %s, (SELECT id FROM loc))
                       RETURNING id;''',
                    (fests.loc_list[i], f[0], f[2][0], f[2][1], f[3], f[-1]))
        f_id = cur.fetchone()[0]
        for b in f[1]:
            cur.execute('''WITH b_id AS (
                               SELECT id FROM band WHERE name = %s LIMIT 1)   
                           INSERT INTO band_festival VALUES ((SELECT id FROM b_id), %s);''',
                        (b, f_id))

    conn.commit()
    
##fill()
##conn.close()

t1 = time.clock()
print(t1-t0)
