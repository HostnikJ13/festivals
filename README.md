# Festivals #

Repozitorij projekta pri predmetu Osnove podatkovnih baz.

Mapa *Data* vsebuje python datoteke za pridobivanje in urejanje podatkov, ustvarjanje tabel ter polnjenje baze.
*App* pa vsebuje vse v zvezi a Shiny R aplikacijo, ki je namenjena iskanju po festivalih in bandih ter prikazuje nekaj grafov.

### ER diagram: ###
![festsER.png](https://bitbucket.org/repo/KpLgq6/images/2837262393-festsER.png)

Avtor: 
Jure Hostnik