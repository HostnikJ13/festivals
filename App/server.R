# This is the server logic for a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com

library(shiny)
library(dplyr)
library(ggmap)
library(ggplot2)

source('auth_public.R')


shinyServer(function(input, output) {
  
  conn <- src_postgres(dbname = db, host = host, user = user, password = password)
  
  tbl.band <- tbl(conn, 'band')
  tbl.festival <- tbl(conn, 'festival')
  tbl.location <- tbl(conn, 'location')
  tbl.band_genre <- tbl(conn, 'band_genre')
  tbl.band_festival <- tbl(conn, 'band_festival')

  
  ### fests ###
  fests <- reactive({left_join(tbl.festival, tbl.location, by = c('location' = 'id')) %>%
      filter(begin_date <= input$dateRange[2] && begin_date >= input$dateRange[1]) %>% data.frame()
  })
  output$festTable <- DT::renderDataTable({
    d <- select(fests(), name.x, begin_date, name.y, country)
    names(d) <- c('Name', 'Date', 'Location', 'Country')
    for (name in names(d)) {
      if (is.character(d[[name]])) {
        Encoding(d[[name]]) <- 'UTF-8'
      } else if (is.factor(d[[name]])) {
        Encoding(levels(d[[name]])) <- 'UTF-8'
      }
    }
    DT::datatable(d, selection = 'single')
  })
  
  output$festInfo1 <- renderUI({
    if (nrow(fests()) > 0 && length(input$festTable_row_last_clicked) > 0) {
      v <- fests()[input$festTable_row_last_clicked,]
      for (name in names(v)) {
        if (is.character(v[[name]])) {
          Encoding(v[[name]]) <- 'UTF-8'
        } else if (is.factor(v[[name]])) {
          Encoding(levels(v[[name]])) <- 'UTF-8'
        }
      }
      
      name <- h2(v$name.x)
      
      date <- if (is.na(v$end_date)) {
                h4(v$begin_date)}
              else {
                h4(v$begin_date, 'to', v$end_date)}
      
      loc <- if (is.na(v$address) && is.na(v$postcode)) {
              p(v$name.y, br(), v$country)}
            else if (is.na(v$address) && !is.na(v$postcode)) {
              p(v$name.y, br(), v$postcode, br(), v$country)}
            else if (is.na(v$postcode)) {
              p(v$name.y, br(), v$address, br(), v$country)}
            else {
              p(v$name.y, br(), v$address, br(), v$postcode, br(), v$country)}
      
      HTML(paste(name, date, loc))
    }
  })

  output$festInfo2 <- renderUI({
    if (nrow(fests()) > 0 && length(input$festTable_row_last_clicked) > 0) {
      v <- fests()[input$festTable_row_last_clicked,]
      for (name in names(v)) {
        if (is.character(v[[name]])) {
          Encoding(v[[name]]) <- 'UTF-8'
        } else if (is.factor(v[[name]])) {
          Encoding(levels(v[[name]])) <- 'UTF-8'
        }
      }
      
      price <- if (!is.na(v$price)) {
        p(strong('Price:'), v$price)}
      
      web <- if (!is.na(v$web)) {
        p(strong('Website:'), a(v$web, href = v$web))}
      
      l <- left_join(tbl.festival, tbl.band_festival, by = c('id' = 'festival')) %>%
        left_join(tbl.band, by = c('band' = 'id')) %>%
        filter(id.x == v$id.x) %>% select(name.y, info) %>% data.frame()
      lineup <- tags$colgroup(strong('Lineup:'), br())
      if (is.na(l)) {
        lineup <- tags$col(lineup, 'No information about lineup yet.')
      }
      else {
        n <- 0
        for(i in 1:nrow(l)) {
          row <- l[i,]
          if (n > 0) {lineup <- tags$col(lineup, '+')}
          if (!is.na(row[[2]])) {
            lineup <- tags$col(lineup, a(row[[1]], href = row[[2]]))
          }
          else {lineup <- tags$col(lineup, tags$col(row[[1]]))}
          n <- n + 1
        }
      }
      
      HTML(paste(br(), br(), price, web, lineup))
    }
  })
  
  output$map <- renderPlot({
    if (nrow(fests()) > 0 && length(input$festTable_row_last_clicked) > 0) {
      v <- fests()[input$festTable_row_last_clicked,]
      
      loc <- if (is.na(v$address) && is.na(v$postcode)) {
                paste(v$name.y, v$country)}
              else if (is.na(v$address) && !is.na(v$postcode)) {
                paste(v$postcode, v$name.y, v$country)}
              else if (is.na(v$postcode)) {
                paste(v$address, v$name.y, v$country)}
              else {
                paste(v$name.y, v$address, v$postcode, v$country)}
      map <- get_map(loc, source = 'google', zoom = 14, maptype = 'roadmap')
      ggmap(map, fullpage = TRUE)
    }
  })
  
  ### bands ###
  bands <- reactive({
    td <- Sys.Date()
    if (input$active && !input$wshow) {
      tbl.band %>% filter(status == 'Active') %>% collect() %>% data.frame()
    }
    else if (!input$active && !input$wshow) {
      tbl.band %>% collect() %>% data.frame()
    }
    else if (input$active && input$wshow) {
      left_join(tbl.festival, tbl.band_festival, by = c('id' = 'festival')) %>%
        left_join(tbl.band, by = c('band' = 'id')) %>%
        filter(begin_date >= td && status == 'Active') %>%
        rename(name = name.y, id = id.y) %>% 
        collect() %>% distinct(name) %>% data.frame()
    }
    else {
      left_join(tbl.festival, tbl.band_festival, by = c('id' = 'festival')) %>%
        left_join(tbl.band, by = c('band' = 'id')) %>%
        filter(begin_date >= td) %>%
        rename(name = name.y, id = id.y) %>% 
        collect() %>% distinct(name) %>% data.frame()
    }
  })
  
  output$bandTable <- DT::renderDataTable({
    d <- select(bands(), name, status, country)
    names(d) <- c('Name', 'Status', 'Country')
    for (name in names(d)) {
      if (is.character(d[[name]])) {
        Encoding(d[[name]]) <- 'UTF-8'
      } else if (is.factor(d[[name]])) {
        Encoding(levels(d[[name]])) <- 'UTF-8'
      }
    }
    DT::datatable(d, selection = 'single')
  })
  
  output$bandInfo <- renderUI({
    if (nrow(bands()) > 0 && length(input$bandTable_row_last_clicked) > 0) {
      v <- bands()[input$bandTable_row_last_clicked,]
      for (name in names(v)) {
        if (is.character(v[[name]])) {
          Encoding(v[[name]]) <- 'UTF-8'
        } else if (is.factor(v[[name]])) {
          Encoding(levels(v[[name]])) <- 'UTF-8'
        }
      }
      
      nname <- h2(v$name)
      
      g <- left_join(tbl.band, tbl.band_genre, by = c('id' = 'band')) %>%
        filter(id == v$id) %>% select(genre) %>% data.frame()
      for (name in names(g)) {
        if (is.character(g[[name]])) {
          Encoding(g[[name]]) <- 'UTF-8'
        } else if (is.factor(g[[name]])) {
          Encoding(levels(g[[name]])) <- 'UTF-8'
        }
      }
      genre <- p("No information about band's genre.")
      if (!is.na(g)) {
        gg <- list(strong('Genres:'))
        for (i in 1:length(g$genre)) {
          gg[[i+1]] <- tags$li(g$genre[i])
        }
        genre <- tags$ul(gg)
      }
      
      info <- if (!is.na(v$info)) {
          p('You can read more about this band on ',
            a('the Encyclopaedia Metallum website', href = v$info), '.')
      }
      else {p('This band is not listed in ',
              a('the Encyclopaedia Metallum', href='http://www.metal-archives.com/'))
      }
      
      td <- Sys.Date()
      su <- left_join(tbl.festival, tbl.band_festival, by = c('id' = 'festival')) %>%
        left_join(tbl.band, by = c('band' = 'id')) %>%
        filter(id.y == v$id && begin_date >= td) %>% 
        select(name.x, web) %>% data.frame()
      for (name in names(su)) {
        if (is.character(su[[name]])) {
          Encoding(su[[name]]) <- 'UTF-8'
        } else if (is.factor(su[[name]])) {
          Encoding(levels(su[[name]])) <- 'UTF-8'
        }
      }
      s <- left_join(tbl.festival, tbl.band_festival, by = c('id' = 'festival')) %>%
        left_join(tbl.band, by = c('band' = 'id')) %>%
        filter(id.y == v$id && begin_date < td) %>% 
        select(name.x, web) %>% data.frame()
      for (name in names(s)) {
        if (is.character(s[[name]])) {
          Encoding(s[[name]]) <- 'UTF-8'
        } else if (is.factor(s[[name]])) {
          Encoding(levels(s[[name]])) <- 'UTF-8'
        }
      }
      shu <- h4('Upcoming shows:')
      sh <- h4('Old festival performances:')
      showsu <- list()
      shows <- list()
      if (length(su) > 0) {
        for(i in 1:nrow(su)) {
          row <- su[i,]
          if (!is.na(row[[2]])) {
            showsu[[i]] <- tags$li(a(row[[1]], href = row[[2]]))
          }
          else {showsu[[i]] <- tags$li(row[[1]])}
        }
        showsu <- tags$ol(showsu)
      }
      else {
        showsu <- 'no shows announced'
      }
      if (length(s) > 0) {
        for(i in 1:nrow(s)) {
          row <- s[i,]
          if (!is.na(row[[2]])) {
            shows[[i]] <- tags$li(a(row[[1]], href = row[[2]]))
          }
          else {shows[[i]] <- tags$li(row[[1]])}
        }
        shows <- tags$ol(rev(shows))
      }
      else {
        shows <- 'no old shows'
      }
      HTML(paste(nname, genre, info, br(), shu, showsu, br(), sh, shows))
    }
  })
  
  ### other ###
  output$bCountry <- renderPlot({
    bc <- tbl.band %>% select(name, country) %>% group_by(country) %>%
      summarise(count=count(country)) %>% filter(count >= 1500) %>%
      collect() %>% data.frame()
    ggplot(data=bc,
           aes(x=country, y=count, fill=country)) +
      geom_bar(stat='identity') +
      scale_x_discrete(breaks=NULL) +
      labs(x='Country', y='Number of bands', fill='')
  })
  
  output$fCountry <- renderPlot({
    fc <- left_join(tbl.festival, tbl.location, by = c('location' = 'id')) %>% 
      select(name.x, country) %>% group_by(country) %>%
      summarise(count=count(country)) %>% filter(count >= 15) %>%
      collect() %>% data.frame()
    ggplot(data=fc,
           aes(x=country, y=count, fill=country)) +
      geom_bar(stat='identity') +
      scale_x_discrete(breaks=NULL) +
      labs(x='Country', y='Number of festivals', fill='')
  })
  
  output$bGenre <- renderPlot({
    bg <- left_join(tbl.band, tbl.band_genre, by = c('id' = 'band')) %>%
      select(name, genre) %>% group_by(genre) %>%
      summarise(count=count(genre)) %>% filter(count >= 800) %>%
      collect() %>% data.frame()
    ggplot(data=bg,
           aes(x=genre, y=count, fill=genre)) +
      geom_bar(stat='identity') +
      scale_x_discrete(breaks=NULL) +
      labs(x='Genre', y='Number of bands', fill='')
  })
})